This project uses Express framework.  
Data are saved to the `/server/db/storage.json` file.


**There are few thing that can be potentially improved/added:**  
General:  
 - api versioning  
 - add `.env` file  
 - endpoint validation  
 - string sanitization in request from client  
 - in PATCH endpoint, it good to return the updated object  

Storage:  
 - in case of storing this application on eg. Heroku,  
  the storage.json isn't persistent  
 - the storage is vulnerable for saving invalida data  
  To save data we need 2 not atomic actions like `readFile` and `writeFile`  
 - when it saves/reads data it needs to parse the whole json file  
  

Errors:  
 - errors tracker (eg. Rollbar)  
 - at the moment every error message is returned to the client,  
  but it is better to control what errors we send to the front-end
