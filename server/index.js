const http = require('http');
const express = require('express');
const cors = require('cors');

const app = express();
app.use(cors({
    origin: 'http://localhost:3000',
}));
app.use(express.json());

app.use('/issues', require('./api/issues'));

app.use((req, res) => {
    res.send('Not found!');
});

const server = http.createServer(app);

const PORT = 3001;
server.listen(PORT, () => console.log(`Issue tracker API listening on port: ${PORT}!`));
