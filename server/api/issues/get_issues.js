const Issue = require('../../models/issue/');

module.exports = async () => ({
    status: 200,
    data: {
        issues: await Issue.getAll(),
    },
});
