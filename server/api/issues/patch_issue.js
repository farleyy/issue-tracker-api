const Issue = require('../../models/issue/');

module.exports = async ({ params, body }) => {
    const { id } = params;
    const { state } = body;

    const issueProps = await Issue.findById(id);
    const issue = new Issue({ id, ...issueProps });

    issue.setState(state);

    issue.save();

    return {
        status: 200,
    };
};
