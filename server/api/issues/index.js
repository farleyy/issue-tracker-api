const router = require('express').Router();
const errorable = require('../../errorable');

router.get('/', errorable(require('./get_issues')));
router.patch('/:id', errorable(require('./patch_issue')));

module.exports = router;
