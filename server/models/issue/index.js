const db = require('../../db/');

const states = ['open', 'pending', 'closed'];

class Issue {
    constructor({
        id, title, description, state,
    }) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.state = state;
    }

    setState(newState) {
        const indexOfNewState = states.indexOf(newState);
        const indexOfCurrentState = states.indexOf(this.state);

        if (indexOfNewState === -1) {
            throw new Error(`State ${newState} doesn't exist!`);
        }

        if (indexOfNewState >= indexOfCurrentState) {
            this.state = newState;
            return;
        }

        throw new Error(`Can't change state from ${this.state} to ${newState}`);
    }

    async save() {
        const { id, ...rest } = this;
        await db.update('issues', id, rest);
    }

    static async findById(id) {
        return db.getById('issues', id);
    }

    static async getAll() {
        const issues = await db.getTable('issues');
        return Object.keys(issues).map((id) => ({
            id,
            ...issues[id],
        }));
    }
}

module.exports = Issue;
