module.exports = function errorable(fn) {
    return async function (req, res, next) {
        try {
            const { status, data = {}, meta = null } = await fn(req, res);
            res.status(status).json({
                status,
                meta,
                data,
            });
        } catch (error) {
            res.status(500).json({
                status: 500,
                data: {
                    errorMessage: error.message,
                },
            });
        }
    };
};
