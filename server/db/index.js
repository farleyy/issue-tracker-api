const fs = require('fs');
const path = require('path');

class Database {
    constructor(pathToStorage) {
        this.id = Math.random();
        this.pathToStorage = path.join(__dirname, pathToStorage);
    }

    async getTable(tableName) {
        const storage = await this.getStorage();
        return storage[tableName];
    }

    async getById(tableName, id) {
        const storage = await this.getStorage();
        return storage[tableName][id];
    }

    async update(table, id, object) {
        const storage = await this.getStorage();
        storage[table][id] = object;
        await this.saveStorage(storage);
    }

    async getStorage() {
        return new Promise((resolve, reject) => {
            fs.readFile(this.pathToStorage, (err, data) => {
                if (err) reject(err);

                resolve(JSON.parse(data));
            });
        });
    }

    async saveStorage(data) {
        return new Promise((resolve, reject) => {
            fs.writeFile(
                this.pathToStorage,
                JSON.stringify(data, null, 2),
                (err) => { if (err) reject(err); },
            );
        });
    }
}


const db = new Database('/storage.json');
module.exports = db;
